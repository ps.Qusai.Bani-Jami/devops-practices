#!/bin/bash

date=$(git log -1 --date=format:"%y%m%d" --format="%ad")
hash=$(git rev-parse HEAD | cut -c -8)

new_version="${date}-${hash}"

mvn build-helper:parse-version versions:set -DnewVersion="${new_version}" -DgenerateBackupPoms=false

echo ${new_version}
